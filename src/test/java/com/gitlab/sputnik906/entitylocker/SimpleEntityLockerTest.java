package com.gitlab.sputnik906.entitylocker;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SimpleEntityLockerTest {

  IEntityLocker<Integer> locker;

  @BeforeEach
  public void setUp() {
    locker = new SimpleEntityLocker<>();
  }

  @Test
  public void shouldGuaranteeThatAtMostOneThreadExecutesProtectedCodeOnEntityIdTest() {

    final int entityId = 1;

    AtomicBoolean resultOfTryExecuteFromAnotherThread = new AtomicBoolean(true);

    locker.execute(entityId,()->sneakyThrow(()->
      createAndStartNewTread(()->
        resultOfTryExecuteFromAnotherThread.set(locker.tryExecute(entityId,()->{}))
      ).join()
    ));

    assertFalse(resultOfTryExecuteFromAnotherThread.get());

  }

  @Test
  public void shouldAllowConcurrentExecutionOfProtectedCodeOnDifferentEntitiesTest(){

    final int entityId1 = 1;
    final int entityId2 = 2;

    AtomicBoolean resultOfTryExecuteFromAnotherThread = new AtomicBoolean(false);

    locker.execute(entityId1,()->sneakyThrow(()->
      createAndStartNewTread(()->
        resultOfTryExecuteFromAnotherThread.set(locker.tryExecute(entityId2,()->{}))
      ).join()
    ));

    assertTrue(resultOfTryExecuteFromAnotherThread.get());

  }

  @Test
  public void shouldAllowReentrantLockingTest(){

    final int entityId1 = 1;

    locker.execute(entityId1, ()->
      sneakyThrow(()->
        assertTrue(locker.tryExecute(entityId1,()->{}))
      )
    );

  }

  @Test
  public void shouldGlobalLockNotExecuteConcurrentlyWithAnyOtherProtectedCodeTest()
    throws InterruptedException {

    final int entityId1 = 1;

    AtomicBoolean resultOfTryExecuteFromAnotherThread = new AtomicBoolean(true);

    locker.executeGlobal(()->sneakyThrow(()->
      createAndStartNewTread(()->
        resultOfTryExecuteFromAnotherThread.set(locker.tryExecute(entityId1,()->{}))
      ).join()
    ));

    assertFalse(resultOfTryExecuteFromAnotherThread.get());

    resultOfTryExecuteFromAnotherThread.set(true);

    locker.execute(entityId1,()->sneakyThrow(()->
      createAndStartNewTread(()->
        resultOfTryExecuteFromAnotherThread.set(locker.tryExecuteGlobal(()->{}))
      ).join()
    ));

    assertFalse(resultOfTryExecuteFromAnotherThread.get());

    resultOfTryExecuteFromAnotherThread.set(false);

    final long estimateFluctuations = 70;
    final long modelTimeWorkGlobal = 150;
    final long modelTimeWorkEntity = 200;
    final long startTime = System.currentTimeMillis();

    createAndStartNewTread(()->locker.executeGlobal(()->sneakyThrow(()-> {
      createAndStartNewTread(()->locker.execute(entityId1,()->sneakyThrow(()-> {
        createAndStartNewTread(()->sneakyThrow(()-> {
          if (!locker.tryExecute(entityId1,()->{})){// if the entity is not locked it is incorrect
            long timePassed = System.currentTimeMillis()-startTime;
            long minEstimateHowLongCanGetAccess = modelTimeWorkGlobal+modelTimeWorkEntity-timePassed+estimateFluctuations;
            resultOfTryExecuteFromAnotherThread.set(locker.tryExecute(entityId1, () -> {
            }, minEstimateHowLongCanGetAccess, TimeUnit.MILLISECONDS));
          }
          synchronized (this) {this.notify();}
        }));
        Thread.sleep(modelTimeWorkEntity);
      })));
      Thread.sleep(modelTimeWorkGlobal);
    })));

    synchronized (this){this.wait(1000);}

    assertTrue(resultOfTryExecuteFromAnotherThread.get());

  }

  @Test
  public void shouldEscalationItsLockToBeGlobalLockTest() {
    SimpleEntityLocker<Integer> localLocker = new SimpleEntityLocker<>(2);

    AtomicBoolean resultOfTryExecuteGlobalFromAnotherThread = new AtomicBoolean(true);
    AtomicBoolean resultOfTryExecuteLocalFromAnotherThread = new AtomicBoolean(true);

    AtomicBoolean resultOfTryExecuteLocal = new AtomicBoolean(false);
    AtomicBoolean resultOfTryExecuteGlobal = new AtomicBoolean(false);


    localLocker.execute(1,
      ()->localLocker.execute(2,
        ()->localLocker.execute(3,()->sneakyThrow(()-> {
          createAndStartNewTread(() -> resultOfTryExecuteGlobalFromAnotherThread.set(
            localLocker.tryExecuteGlobal(() -> { })
          )).join();
          createAndStartNewTread(() -> resultOfTryExecuteLocalFromAnotherThread.set(
            localLocker.tryExecute(4,() -> { })
          )).join();
          resultOfTryExecuteLocal.set(localLocker.tryExecute(5,()->{}));
          resultOfTryExecuteGlobal.set(localLocker.tryExecuteGlobal(()->{}));
        }))
      )
    );

    assertFalse(resultOfTryExecuteGlobalFromAnotherThread.get());
    assertFalse(resultOfTryExecuteLocalFromAnotherThread.get());

    assertTrue(resultOfTryExecuteLocal.get());
    assertTrue(resultOfTryExecuteGlobal.get());
  }


  @Test
  public void shouldImplementProtectionFromDeadlocks(){
    final int entityId1 = 1;
    final int entityId2 = 2;

    locker.execute(entityId1,()->sneakyThrow(()->{
      createAndStartNewTread(()->
        locker.execute(entityId2,()->locker.execute(entityId1,()->{}))
      );
      Thread.sleep(100);
      assertThrows(DetectDeadLockException.class,()->
        locker.tryExecute(entityId2,()->{},100, TimeUnit.MILLISECONDS)
      );
    }));


    AtomicBoolean resultOfTryExecuteLocalInGlobalLock = new AtomicBoolean(false);

    locker.executeGlobal(()->resultOfTryExecuteLocalInGlobalLock.set(locker.tryExecute(entityId1,()->{})));

    assertTrue(resultOfTryExecuteLocalInGlobalLock.get());

    AtomicBoolean resultOfTryExecuteGlobalInLocalLock = new AtomicBoolean(false);

    locker.execute(entityId1,()->resultOfTryExecuteGlobalInLocalLock.set(locker.tryExecuteGlobal(()->{})));

    assertTrue(resultOfTryExecuteGlobalInLocalLock.get());
  }

  @Test
  public void loadTest() throws InterruptedException {
    final int countOfUnsafeObjects = 8;
    final int countOfRunUnsafeObject = 8;
    final long countOfIncrementPerOneRun = 1000000000L;

    ExecutorService executor = Executors.newFixedThreadPool(8);

    List<Callable<Integer>> tasks = new ArrayList<>();

    Map<Integer,UnsafeCounter> entityMap = new ConcurrentHashMap<>();
    IntStream.range(0,countOfUnsafeObjects)
      .forEach(entityId->entityMap.put(entityId,new UnsafeCounter(countOfIncrementPerOneRun)));
    IntStream.range(0,countOfUnsafeObjects)
      .forEach(entityId->tasks.addAll(
        IntStream.range(0,countOfRunUnsafeObject)
          .mapToObj(ii->(Callable<Integer>)()->locker.execute(entityId,()->{
            entityMap.get(entityId).run();
            return entityId;
          }))
          .collect(Collectors.toList())
      ));

    Collections.shuffle(tasks,new Random(0));

    executor.invokeAll(tasks,5000,TimeUnit.MILLISECONDS);

    IntStream.range(0,countOfUnsafeObjects).forEach(entityId->
      assertFalse(
        entityMap.get(entityId).getHasBrokenConcurrent().get()
      )
    );

  }



  private static class UnsafeCounter{
    private final long countOfIncrementPerOneRun;

    private final ReentrantReadWriteLock rw = new ReentrantReadWriteLock();

    private final AtomicBoolean hasBrokenConcurrent = new AtomicBoolean(false);

    private long counter=0;

    private UnsafeCounter(long countOfIncrementPerOneRun) {
      this.countOfIncrementPerOneRun = countOfIncrementPerOneRun;
    }

    public void run(){
      rw.readLock().lock();
      try{
        if (rw.getReadLockCount()!=1) hasBrokenConcurrent.set(true);
        for(int i=0; i<countOfIncrementPerOneRun; i++) counter++;
      }finally {
        rw.readLock().unlock();
      }
    }

    public AtomicBoolean getHasBrokenConcurrent() {
      return hasBrokenConcurrent;
    }
  }

  private Thread createAndStartNewTread(RunnableThrowException runnable){
    Thread thread = new Thread(()->sneakyThrow(runnable));
    thread.start();
    return thread;
  }

  private void sneakyThrow(RunnableThrowException runnable){
    try{
      runnable.run();
    }catch (Throwable e){
      throw new IllegalStateException(e);
    }
  }

  @FunctionalInterface
  private interface RunnableThrowException{
    void run() throws Throwable;
  }


}
