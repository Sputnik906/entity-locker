package com.gitlab.sputnik906.entitylocker;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * An implementation of the interface provides synchronization mechanisms
 * @param <T> - type of entity id
 */
public class SimpleEntityLocker<T> implements IEntityLocker<T> {

  private final long numberOfLocksNeededToGetGlobal;

  private final ConcurrentMap<T, ReentrantLockWithCounterUsage> reentrantLockMap = new ConcurrentHashMap<>();

  private final ReentrantReadWriteLock globalLock = new ReentrantReadWriteLock();

  public SimpleEntityLocker(long numberOfLocksNeededToGetGlobal) {
    this.numberOfLocksNeededToGetGlobal = numberOfLocksNeededToGetGlobal;
  }

  public SimpleEntityLocker() {
    this(-1);//without escalate
  }

  @Override
  public void execute(T id, Runnable protectedCode) {
    Objects.requireNonNull(id,"id should be not null");
    Objects.requireNonNull(protectedCode,"protectedCode should be not null");

    if (globalLock.isWriteLockedByCurrentThread()){
      protectedCode.run();
      return;
    }

    boolean alreadyCurrentThreadHoldReadLock = globalLock.getReadHoldCount()>0;

    if (doesCurrentThreadHaveTooManyLocks()){
      if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
      try{
        executeGlobal(protectedCode);
        return;
      }finally {
        if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().lock();
      }
    }


    if (!alreadyCurrentThreadHoldReadLock) globalLock.readLock().lock();

    try{
      ReentrantLock lock = getLockInstanceAndIncreaseCounter(id);

      lock.lock();
      try{
        protectedCode.run();
      }finally {
        lock.unlock();
        tryRemoveLockInstance(id);
      }
    }finally {
      if (!alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
    }
  }

  @Override
  public <R> R execute(T id, Supplier<R> protectedCode){
    Objects.requireNonNull(id,"id should be not null");
    Objects.requireNonNull(protectedCode,"protectedCode should be not null");

    if (globalLock.isWriteLockedByCurrentThread()){
      return protectedCode.get();
    }

    boolean alreadyCurrentThreadHoldReadLock = globalLock.getReadHoldCount()>0;

    if (doesCurrentThreadHaveTooManyLocks()){
      if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
      try{
        return executeGlobal(protectedCode);
      }finally {
        if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().lock();
      }
    }

    if (!alreadyCurrentThreadHoldReadLock) globalLock.readLock().lock();

    try{
      ReentrantLock lock = getLockInstanceAndIncreaseCounter(id);

      lock.lock();
      try{
        return protectedCode.get();
      }finally {
        lock.unlock();
        tryRemoveLockInstance(id);
      }

    }finally {
      if (!alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
    }
  }

  @Override
  public boolean tryExecute(T id, Runnable protectedCode) {
    Objects.requireNonNull(id,"id should be not null");
    Objects.requireNonNull(protectedCode,"protectedCode should be not null");

    if (globalLock.isWriteLockedByCurrentThread()){
      protectedCode.run();
      return true;
    }

    boolean alreadyCurrentThreadHoldReadLock = globalLock.getReadHoldCount()>0;

    if (doesCurrentThreadHaveTooManyLocks()){
      if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
      try{
        return tryExecuteGlobal(protectedCode);
      }finally {
        if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().lock();
      }
    }

    if (alreadyCurrentThreadHoldReadLock||globalLock.readLock().tryLock()){
      try{
        ReentrantLock lock = getLockInstanceAndIncreaseCounter(id);

        if (lock.tryLock()){
          try{
            protectedCode.run();
            return true;
          }finally {
            lock.unlock();
            tryRemoveLockInstance(id);
          }
        }

      }finally {
        if (!alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
      }
    }

    return false;
  }

  @Override
  public boolean tryExecute(T id, Runnable protectedCode, long timeout,
    TimeUnit unit) throws InterruptedException {

    Objects.requireNonNull(id,"id should be not null");
    Objects.requireNonNull(protectedCode,"protectedCode should be not null");
    Objects.requireNonNull(unit,"unit should be not null");

    if (globalLock.isWriteLockedByCurrentThread()){
      protectedCode.run();
      return true;
    }

    boolean alreadyCurrentThreadHoldReadLock = globalLock.getReadHoldCount()>0;

    if (doesCurrentThreadHaveTooManyLocks()){
      if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
      try{
        return tryExecuteGlobal(protectedCode,timeout,unit);
      }finally {
        if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().lock();
      }
    }

    long startTime = System.currentTimeMillis();

    if (alreadyCurrentThreadHoldReadLock||globalLock.readLock().tryLock(timeout,unit)){
      try{
        ReentrantLock lock = getLockInstanceAndIncreaseCounter(id);

        long timePassed = System.currentTimeMillis()-startTime;
        if (lock.tryLock(timeout-timePassed,unit)){
          try{
            protectedCode.run();
            return true;
          }finally {
            lock.unlock();
            tryRemoveLockInstance(id);
          }
        }
      }finally {
        if (!alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
      }
    }

    return false;

  }

  @Override
  public <R> R tryExecute(T id, Supplier<R> protectedCode) {
    Objects.requireNonNull(id,"id should be not null");
    Objects.requireNonNull(protectedCode,"protectedCode should be not null");

    if (globalLock.isWriteLockedByCurrentThread()){
      return protectedCode.get();
    }

    boolean alreadyCurrentThreadHoldReadLock = globalLock.getReadHoldCount()>0;

    if (doesCurrentThreadHaveTooManyLocks()){
      if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
      try{
        return executeGlobal(protectedCode);
      }finally {
        if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().lock();
      }
    }

    if (alreadyCurrentThreadHoldReadLock||globalLock.readLock().tryLock()){
      try{
        ReentrantLock lock = getLockInstanceAndIncreaseCounter(id);

        if (lock.tryLock()){
          try{
            return protectedCode.get();
          }finally {
            lock.unlock();
            tryRemoveLockInstance(id);
          }
        }
      }finally {
        if (!alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
      }
    }

    return null;
  }

  @Override
  public <R> R tryExecute(T id, Supplier<R> protectedCode, long timeout,
    TimeUnit unit) throws InterruptedException {

    Objects.requireNonNull(id,"id should be not null");
    Objects.requireNonNull(protectedCode,"protectedCode should be not null");
    Objects.requireNonNull(unit,"unit should be not null");

    if (globalLock.isWriteLockedByCurrentThread()){
      return protectedCode.get();
    }

    boolean alreadyCurrentThreadHoldReadLock = globalLock.getReadHoldCount()>0;

    if (doesCurrentThreadHaveTooManyLocks()){
      if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
      try{
        return executeGlobal(protectedCode);
      }finally {
        if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().lock();
      }
    }

    long startTime = System.currentTimeMillis();

    if (alreadyCurrentThreadHoldReadLock||globalLock.readLock().tryLock(timeout,unit)) {
      try {
        ReentrantLock lock = getLockInstanceAndIncreaseCounter(id);

        long timePassed = System.currentTimeMillis()-startTime;
        if (lock.tryLock(timeout-timePassed,unit)){
          try{
            return protectedCode.get();
          }finally {
            lock.unlock();
            tryRemoveLockInstance(id);
          }
        }
      }finally {
        if (!alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
      }
    }
    return null;
  }

  @Override
  public void executeGlobal(Runnable protectedCode) {
    boolean alreadyCurrentThreadHoldReadLock = globalLock.getReadHoldCount()>0;
    if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
    globalLock.writeLock().lock();
    try{
      protectedCode.run();
    }finally {
      if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().lock();
      globalLock.writeLock().unlock();
    }

  }

  @Override
  public <R> R executeGlobal(Supplier<R> protectedCode) {
    boolean alreadyCurrentThreadHoldReadLock = globalLock.getReadHoldCount()>0;
    if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
    globalLock.writeLock().lock();
    try{
      return protectedCode.get();
    }finally {
      if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().lock();
      globalLock.writeLock().unlock();
    }
  }

  @Override
  public boolean tryExecuteGlobal(Runnable protectedCode) {
    boolean alreadyCurrentThreadHoldReadLock = globalLock.getReadHoldCount()>0;
    if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
    if (globalLock.writeLock().tryLock()){
      try{
        protectedCode.run();
        return true;
      }finally {
        if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().lock();
        globalLock.writeLock().unlock();
      }
    }

    return false;
  }

  @Override
  public <R> R tryExecuteGlobal(Supplier<R> protectedCode) {
    boolean alreadyCurrentThreadHoldReadLock = globalLock.getReadHoldCount()>0;
    if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();
    if (globalLock.writeLock().tryLock()){
      try{
        return protectedCode.get();
      }finally {
        if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().lock();
        globalLock.writeLock().unlock();
      }
    }

    return null;
  }

  @Override
  public boolean tryExecuteGlobal(Runnable protectedCode, long timeout,
    TimeUnit unit) throws InterruptedException {

    boolean alreadyCurrentThreadHoldReadLock = globalLock.getReadHoldCount()>0;
    if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();

    if (globalLock.writeLock().tryLock(timeout,unit)){
      try{
        protectedCode.run();
        return true;
      }finally {
        if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().lock();
        globalLock.writeLock().unlock();
      }
    }

    return false;
  }

  @Override
  public <R> R tryExecuteGlobal(Supplier<R> protectedCode, long timeout,
    TimeUnit unit) throws InterruptedException {

    boolean alreadyCurrentThreadHoldReadLock = globalLock.getReadHoldCount()>0;
    if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().unlock();

    if (globalLock.writeLock().tryLock(timeout,unit)){
      try{
        return protectedCode.get();
       }finally {
        if (alreadyCurrentThreadHoldReadLock) globalLock.readLock().lock();
        globalLock.writeLock().unlock();
      }
    }

    return null;
  }

  private boolean doesCurrentThreadHaveTooManyLocks(){
    if (numberOfLocksNeededToGetGlobal<0) return false;
    return reentrantLockMap.values().stream()
      .filter(ReentrantLock::isHeldByCurrentThread)
      .count()>=numberOfLocksNeededToGetGlobal;
  }


  private ReentrantLock getLockInstanceAndIncreaseCounter(T id){
    return reentrantLockMap.compute(id,(k,lock)-> {
      List<ReentrantLockWithCounterUsage> potentialLocksForDeadLock = getCurrentThreadPotentialLocksForDeadLock(lock);
      if (potentialLocksForDeadLock==null){
        return Optional.ofNullable(lock).orElse(new ReentrantLockWithCounterUsage())
          .holdCurrentThreadAndReturnInstance();
      }else{
        synchronized (this){
          potentialLocksForDeadLock.stream()
            .filter(activeLock->activeLock.containAnyOfTreads(lock.holderThreads()))
            .findFirst()
            .ifPresent(p->{throw new DetectDeadLockException();});
          return lock.holdCurrentThreadAndReturnInstance();
        }

      }
    });
  }

  /**
   * The main idea is that before calling the lock() method on the ReentrantLockWithCounterUsage instance, we need
   * make sure that the thread that owns the lock is not the thread that is in
   * heldThreads for ReentrantLockWithCounterUsage instances where the current thread is the owner
   * @param checkedLock what we want to check for deadlocks when trying to call its lock methods
   * @return list of current thread locks
   */
  private List<ReentrantLockWithCounterUsage> getCurrentThreadPotentialLocksForDeadLock(ReentrantLockWithCounterUsage checkedLock){
    if (checkedLock==null) return null;
    if (checkedLock.isHeldByCurrentThread()) return null; //reenter
    if (checkedLock.holderThreads().size()==0) return null;
    List<ReentrantLockWithCounterUsage> currentThreadLocks =  reentrantLockMap.values().stream()
      .filter(ReentrantLock::isHeldByCurrentThread)
      .collect(Collectors.toList());

    return currentThreadLocks.isEmpty()
      ?null
      :currentThreadLocks;
  }

  private void tryRemoveLockInstance(T id){
    reentrantLockMap.computeIfPresent(id,(k,r)->r.removeCurrentThreadAndGetRestOfHeldThread()==0?null:r);
  }

  private static class ReentrantLockWithCounterUsage extends ReentrantLock{

    private final Set<Thread> heldThreads = ConcurrentHashMap.newKeySet();

    public ReentrantLockWithCounterUsage holdCurrentThreadAndReturnInstance(){
      heldThreads.add(Thread.currentThread());
      return this;
    }

    public long removeCurrentThreadAndGetRestOfHeldThread(){
      heldThreads.remove(Thread.currentThread());
      return heldThreads.size();
    }

    public boolean containAnyOfTreads(Collection<Thread> threads){
      return threads.stream()
         .anyMatch(heldThreads::contains);
    }

    public Collection<Thread> holderThreads(){
      return Collections.unmodifiableCollection(heldThreads);
    }

  }
}
