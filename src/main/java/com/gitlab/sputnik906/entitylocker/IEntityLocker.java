package com.gitlab.sputnik906.entitylocker;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * The interface provides synchronization mechanisms for executing code that should have
 * exclusive access to the entity (called “protected code”)
 * @param <T> - type of entity id
 */
public interface IEntityLocker<T> {

  /**
   * Executes protected code for a given entity id
   * @param id - entity id
   * @param protectedCode - protected code
   */
  void execute(T id, Runnable protectedCode);

  /**
   * Executes protected code with the returned result for a given entity id
   * @param id - entity id
   * @param protectedCode - protected code
   * @param <R> - type of result of protected code
   * @return the result of executing the protected code
   */
  <R> R execute(T id, Supplier<R> protectedCode);

  /**
   * Tries to execute protected code immediately for a given entity id
   * @param id - entity id
   * @param protectedCode - protected code
   * @return true if the code was executed immediately otherwise false
   */
  boolean tryExecute(T id,Runnable protectedCode);

  /**
   * Tries to execute protected code within the specified timeout for a given entity id
   * @param id - entity id
   * @param protectedCode - protected code
   * @param timeout - timeout
   * @param unit - unit
   * @return true if the code was executed within the specified timeout otherwise false
   * @throws InterruptedException if current thread is interrupted
   */
  boolean tryExecute(T id,Runnable protectedCode,long timeout, TimeUnit unit) throws InterruptedException;

  /**
   * Tries to execute protected code with the returned result immediately for a given entity id
   * @param id - entity id
   * @param protectedCode - protected code
   * @param <R> - type of result of protected code
   * @return the result of executing the protected code or null if could not take the lock immediately
   * In this use case you cannot distinguish between situations where the code was executed and returned null or where it was not executed
   */
  <R> R tryExecute(T id,Supplier<R> protectedCode);

  /**
   * Tries to execute protected code with the returned result for a given id entity for a specified timeout
   * @param id - entity id
   * @param protectedCode - protected code
   * @param timeout - timeout
   * @param unit - unit
   * @param <R> - type of result of protected code
   * @return the result of executing the protected code or null if could not take the lock within specified timeout
   * In this use case you cannot distinguish between situations where the code was executed and returned null or where it was not executed
   * @throws InterruptedException if current thread is interrupted
   */
  <R> R tryExecute(T id, Supplier<R> protectedCode,long timeout, TimeUnit unit) throws InterruptedException;

  /**
   * Executes protected code under a global lock
   * @param protectedCode - protected code
   */
  void executeGlobal(Runnable protectedCode);


  /**
   * Executes protected code with the returned result under a global lock
   * @param protectedCode - protected code
   */
  <R> R executeGlobal(Supplier<R> protectedCode);

  /**
   * Tries to execute protected code under a global lock
   * @param protectedCode - protected code
   */
  boolean tryExecuteGlobal(Runnable protectedCode);


  /**
   * Tries to execute protected code with the returned result under a global lock
   * @param protectedCode - protected code
   */
  <R> R tryExecuteGlobal(Supplier<R> protectedCode);

  /**
   * Tries to execute protected code under a global lock for a specified timeout
   * @param protectedCode - protected code
   */
  boolean tryExecuteGlobal(Runnable protectedCode,long timeout, TimeUnit unit) throws InterruptedException;

  /**
   * Tries to execute protected code with the returned result under a global lock for a specified timeout
   * @param protectedCode - protected code
   */
  <R> R tryExecuteGlobal(Supplier<R> protectedCode,long timeout, TimeUnit unit) throws InterruptedException;

}
